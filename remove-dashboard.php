<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Remove_welcome_dashboard
 *
 * @wordpress-plugin
 * Plugin Name:       Remove welcome dashboard
 * Plugin URI:        http://owoyale.com/
 * Description:       This is wordpress plugin removes the welcome message from dashboard.
 * Version:           1.0.0
 * Author:            Femi Yale
 * Author URI:        http://owoyale.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       Remove-welcome-dashboard
 * Domain Path:       /languages
 */

function femiowo_add_google_analytics(){

	global $wp_admin_bar;

	$wp_admin_bar->add_menu(
								array(
									'id' => 'google_analytics',
									'title' => 'Google Analytics',
									'href' => 'http://www.google.com/analytics'

									)
		);
}

add_action('wp_before_admin_bar_render','femiowo_add_google_analytics');